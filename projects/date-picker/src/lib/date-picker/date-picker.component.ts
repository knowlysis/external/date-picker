import { Component, Input, Output, EventEmitter, forwardRef, OnChanges } from '@angular/core';
import { NgbDate, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, NG_VALIDATORS } from '@angular/forms';
@Component({
  selector: 'csd-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    NgbDatepickerConfig,
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatePickerComponent), multi: true},
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => DatePickerComponent), multi: true}
  ]
})
export class DatePickerComponent implements OnChanges, ControlValueAccessor, Validator {
  /** The title for the date field. */
  @Input() title = 'Date Range';

  /** The number of months to display at one time. */
  @Input() months = 2;

  /** Whether the date picker is a single date or a range. Defaults to range. */
  @Input() single = false;

  /** The start date that was passed from the parent component. */
  @Input() dateStart: string;

  /** Emits values for the start date to the parent component. */
  @Output() dateStartChange = new EventEmitter<string>();

  /** Date that is currently hovered. */
  hoveredDate: NgbDate;

  /** The start date. */
  startDate: NgbDate;

  /** The end date. */
  endDate: NgbDate;

  /**
   * Sets the viewable years in the datepicker.
   * @param config Allows configuration of NGB Datepicker.
   */
  constructor(private config: NgbDatepickerConfig) {
    this.config.minDate = {year: 1919, month: 1, day: 1};
    this.endDate = null;
  }

  /**
   * Lifecycle hook that fires when `@Input` items above change.
   */
  ngOnChanges() {
    if (this.dateStart) {
      this.startDate = this.toDateNGB(this.dateStart);
    }
  }

  /**
   * `ControlValueAccessor` helper function for `registerOnChange`.
   */
  onChange = (_: any) => {};

  /**
   * `ControlValueAccessor` helper function for `registerOnTouched` (Unused).
   */
  onTouched = () => {};

  /**
   * `ControlValueAccessor` function that fires when the item bound to the `[(ngModel)]` changes.
   * Updates the end date.
   * @param date The date value passed to `[(ngModel)]` from parent.
   */
  writeValue(date: string) {
    this.endDate = date ? this.toDateNGB(date) : null;
  }

  /**
   * `ControlValueAccessor` function that fires whenever the input is changed and passes the value to the parent.
   * @param fn A function.
   */
  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  /**
   * `ControlValueAccessor` function that fires when the input is touched (Unused).
   * @param fn A function.
   */
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  /**
   * Implements `Validator`
   * Determines if the dates are valid and selected.
   * @returns `null` if valid, failure object if false.
   */
  validate(): {[key: string]: any} {
    if (this.single) {
      return (!this.endDate || !this.endDate.day || !this.endDate.month || !this.endDate.year) && this.endDate ? {validDate: false} : null;
    }
    return (this.startDate && !this.endDate) ? {validDate: false} : null;
  }

  /**
   * Sets the start and/or end date on selection and emits the values.
   * @param date The date that was selected
   */
  onDateSelection(date: NgbDate) {
    if (this.single) {
      this.onChange(this.toDateISO(date));
    } else if (!this.startDate && !this.endDate) {
      this.startDate = date;
      this.dateStartChange.emit(this.toDateISO(this.startDate));
    } else if (this.startDate && !this.endDate && date.after(this.startDate)) {
      this.endDate = date;
      this.onChange(this.toDateISO(this.endDate));
    } else {
      this.endDate = null;
      this.onChange(this.endDate);

      this.startDate = date;
      this.dateStartChange.emit(this.toDateISO(this.startDate));
    }

    this.onChange(this.toDateISO(date));
  }

  /**
   * Determines if the date is hovered.
   * @param date The date to check.
   * @returns True if hovered, else false.
   */
  isHovered(date: NgbDate): boolean {
    return this.startDate && !this.endDate && this.hoveredDate && date.after(this.startDate) && date.before(this.hoveredDate);
  }

  /**
   * Determines if the date is between the start and end dates.
   * @param date The date to check.
   * @returns True if inside, else false.
   */
  isInside(date: NgbDate): boolean {
    return date.after(this.startDate) && date.before(this.endDate);
  }

  /**
   * Determines if the date is in the viewable range.
   * @param date The date to check.
   * @returns True if within, else false.
   */
  isRange(date: NgbDate): boolean {
    return date.equals(this.startDate) || date.equals(this.endDate) || this.isInside(date) || this.isHovered(date);
  }

  /**
   * Converts an ngbDate to an ISO string of a date and returns it.
   * @param date The ngbDate to convert.
   * @returns An ISO string.
   */
  toDateISO(date: NgbDate): string {
    if (!date || !date.day || !date.month || !date.year) {
      return '';
    }
    return new Date(date.year, date.month - 1, date.day).toISOString();
  }

  /**
   * Converts an ISO string to an ngbDate
   * @param date The ISO string to convert.
   * @returns An ngbDate.
   */
  toDateNGB(date: string): NgbDate {
    const tempDate = new Date(date);
    return NgbDate.from({ day: tempDate.getUTCDate(), month: tempDate.getUTCMonth() + 1, year: tempDate.getUTCFullYear()});
  }
}
