import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dateStart: string;
  dateEnd: string;

  dateStartVar: string;
  dateEndVar: string;

  singleDate: string;

  save() {
    alert('The form was saved.');

    this.dateStart = this.dateEnd = this.dateStartVar = this.dateEndVar = this.singleDate = '';
  }
}
